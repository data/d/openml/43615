# OpenML dataset: Esports-matches-in-2020-pre-worlds

https://www.openml.org/d/43615

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
All credit of this database goes to Tim Sevenhuysen of OraclesElixir.com. Im just uploading it here because I want to see what you guys do with this dataset before Worlds! Im super hyped! 
Teams
The qualified teams are: ["G2 Esports", "Fnatic", "Rogue", "MAD Lions",
                "Team SoloMid", "FlyQuest", "Team Liquid", "Top Esports",
                "JD Gaming", "Suning", "LGD Gaming", "DAMWON Gaming",
                "DRX", "Gen.G", "Machi Esports", "PSG Talon", "INTZ",
                "Unicorns Of Love.CIS", "V3 Esports", "Rainbow7", 
                "Legacy Esports", "Papara SuperMassive"]
Go G2.
Content
All pro matches from regular season and playoffs of all pro teams (13 sept. 2020)
Acknowledgements
Again, Tim Sevenhuysen of OraclesElixir.com for creating and updating this dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43615) of an [OpenML dataset](https://www.openml.org/d/43615). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43615/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43615/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43615/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

